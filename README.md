# incommon-docker

This repository contains an opinionated Docker Compose setup for a `development` environment of the [`incommon-api`](https://framagit.org/incommon.cc/incommon-api). This environment documents a convention as an example about how we like to do things, which you can always adapt to your actual needs. 

It is based on the ideas presented in the article [How to use Docker Compose for Rails development](https://anonoz.github.io/tech/2019/03/10/rails-docker-compose-yml.html).

## Prequisites

To use this repository, please refer to the official documentation¹ to set up Docker and Docker Compose on your platform.

These instructions assume you are using an alias for Docker Compose. It can be added to your shell's `.*rc` file.

    alias dc=docker-compose

In cases a `dc` binary is installed, it will remain accessible via its absolute path. Check `type dc` for all known occurrencies.

## Installation

These steps may vary depending on your actual configuration.

### Clone the repository

This repository references the application's source code as a submodule in `src/`.

```
git clone --recursive git@framagit.org:incommon.cc/incommon-docker.git
cd incommon-docker
```

If you omit the `--recursive` flag during `git clone`, make sure to run `git submodule init` from within the repository. See the *Lifecycles* section below for further maintenance commands.

### Prepare environments

To provide an environment for your application, you can refer to the examples and work from there.

```
cp app.env.example app.env
cp postgres.env.example postgres.env
```

In `postgres.env` for `POSTGRES_USER` we simply suggest `incommon`. A sufficiently strong `POSTGRES_PASSWORD` can be generated with `pwgen 32`. The chosen values need to be placed into `DATABASE_URL` in `app.env`.

> *Note 1*: We are using the `DATABASE_URL` without a specified database after the `@postgres` host, in so `rake` tasks work transparently for `development` and `test` environments (the `incommon-api_dev` and `incommon-api_test` databases).

> *Note 2*: In case you are running from a database dump that uses encrypted credentials, you can also provide the `RAILS_MASTER_KEY`. This should not be relevant for development environments, but helps when working with production dumps. Make sure to place your respective `<environment>.yml.env` into `src/config/credentials/` if not available right away.

### Prepare container images

```
dc pull
dc build
```

### Install dependencies and database seeds

```
dc run --rm app bundle install
dc run --rm app bundle exec rails db:setup
```

After this succeeded, you are ready to ingest eventual database dumps. Refer to the *Lifecycles* section below to read how we suggest this to be done.

### Run the application

For development you might want to attach to stdout of a container and be able to kill it with Ctrl + C.

    dc up

Or you can run the processes in the background.

    dc up -d

All docker compose lifecycle commands (`ps`, `up`, `rm`, `build`, `pull`, ...) accept service names as defined in `docker-compose.yml` as parameters.

> Note: The `web` and `app` containers bind to the ports `443` and `3000` on `localhost` `::1`. Please make sure they are reachable and available. Your firewall must be configured accordingly. Open ports can be checked with `ss -tlnp`. When this is not desired, uncomment the `ports` sections in `docker-compose.yml`.

## Lifecycles

This section covers regular maintenance tasks that happen to be useful in development lifecycles.

### Changes to the Gemfile

Each time changes to the Gemfile occur, you can update the on-disk cache of installed Gems with

    dc run --rm app bundle install

### Ingest a database dump

To import a database dump,

    dc up -d postgres
    dc exec -T postgres sh -c 'psql -U ${POSTGRES_USER} -d incommon-api_dev' < incommon_data..sql

### Remove the database

To remove the database you can do

```
dc stop postgres
dc rm -f postgres
sudo rm -rf .state/postgis-data
```

### Recycle containers

In case you need to reset a container, i.e. for resetting the database or for running from an updated code base, you are advised to recycle your (immutable) containers for picking up the changes.

```
dc stop app && dc rm -f app
# do something
dc up -d app && dc logs -f app
```

Or all in one:

    dc stop ; dc rm -f ; dc up -d ; dc logs -f

### Update git submodule

To reset the submodule in `src/` to the upstream branch that matches the one currently checked out from this repository, run

    git submodule update --remote src

## Next

- `test` and `production` configurations and examples

## Notes

¹ [Install Docker Compose | Docker Documentation](https://docs.docker.com/compose/install/)

## License

`incommon-docker` Copyright © 2019 IN COMMON Collective and contributors.

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along
with this program.  If not, see <https://www.gnu.org/licenses/>.

See [LICENSE](../LICENSE.md)

[IN COMMON Collective]: https://talk.incommon.cc/groups/incommon